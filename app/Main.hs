module Main where

import           Alison (startAlison)


dbFile :: FilePath
dbFile = "alison.db"

main :: IO ()
main = startAlison dbFile
