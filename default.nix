{ pkgs ? import <nixpkgs> {} }:
with pkgs;
with haskellPackages;

let
  cabal2nix = src: pkgs.runCommand "cabal2nix" {
    buildCommand = ''
        cabal2nix file://"${builtins.filterSource (path: type: path != ".git") src}" > $out
    '';
    buildInputs = [
        pkgs.cabal2nix
    ];
  } "";
  alison = callPackage (cabal2nix ./.) {};
in 
stdenv.mkDerivation rec {
  name = "alison";
  src = ./.;
  buildInputs = [
    pkgs.zlib
    alison
  ];
  installPhase = ''
    mkdir $out
    cp -r ${alison}/* $out
  '';
}
