{-# LANGUAGE OverloadedStrings #-}

module Alison.Middleware
  ( middleware
  )
where

import           Network.Wai
import           Network.Wai.Middleware.Cors
import           Network.Wai.Middleware.RequestLogger
import           Network.Wai.Middleware.Static

middleware :: Application -> Application
middleware = logStdoutDev . staticPolicy (addBase "static") . cors (const $ Just policy)
 where
  policy = simpleCorsResourcePolicy { corsRequestHeaders = ["content-type"] }
