{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell   #-}

module Alison.Types where

import           Data.Aeson
import           Data.Aeson.TH
import           Data.Text

type Message = Text
data User = User
  { userId        :: Int
  , userFirstName :: String
  , userLastName  :: String
  , userPassword  :: String
  } deriving (Eq, Show)

$(deriveJSON defaultOptions ''User)
