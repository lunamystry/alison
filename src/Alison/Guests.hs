{-# LANGUAGE DeriveGeneric     #-}
{-# LANGUAGE OverloadedLabels  #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell   #-}

module Alison.Guests where

import           Control.Lens
import           Data.Aeson
import           Data.Aeson.Lens
import           Database.Selda
import           Database.Selda.SQLite

data RSVP = Yes | No | NotSure | Unknown
  deriving (Show, Read, Bounded, Enum, Generic)
instance SqlType RSVP
instance FromJSON RSVP
instance ToJSON RSVP

data Guest = Guest
  { email     :: Text
  , firstName :: Text
  , lastName  :: Text
  , password  :: Text
  , rsvp      :: RSVP
  } deriving (Generic, Show)
instance SqlRow Guest
instance FromJSON Guest
instance ToJSON Guest

makeLenses ''Guest

guests :: Table Guest
guests = table "guests" [#email :- primary]

insertGuests :: IO ()
insertGuests = withSQLite "alison.sqlite" $ do
  tryCreateTable guests
  tryInsert
    guests
    [ Guest
            "james@pallo.com"
            "James"
            "Pallo"
            "b4cc344d25a2efe540adbf2678e2304c"
            Unknown {- james -}
    , Guest
            "betty@sims.com"
            "Betty"
            "Sims"
            "82b054bd83ffad9b6cf8bdb98ce3cc2f"
            Yes {- betty -}
    , Guest
            "james@oreily.com"
            "James"
            "O'Reily"
            "b4cc344d25a2efe540adbf2678e2304c"
            No {- james -}
    , Guest
            "sam@sophitz.com"
            "Sam"
            "Sophitz"
            "332532dcfaa1cbf61e2a266bd723612c"
            NotSure {- sam -}
    , Guest
            "sam@jely.com"
            "Sam"
            "Jely"
            "332532dcfaa1cbf61e2a266bd723612c"
            Unknown {- sam -}
    ]

  emailAndName <- query $ do
    guest <- select guests
    return (guest ! #email :*: guest ! #firstName)
  liftIO $ print emailAndName
