{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE TemplateHaskell       #-}
{-# LANGUAGE TypeOperators         #-}

module Alison
  ( startAlison
  , alison
  )
where

import           Control.Concurrent
import           Control.Exception                      (bracket)
import           Control.Lens
import           Control.Monad.IO.Class
import           Crypto.BCrypt
import           Data.Aeson
import           Data.Aeson.Lens
import           Data.Aeson.TH
import           Data.ByteString                        (ByteString)
import           Data.Monoid                            ((<>))
import           Data.Text                              (Text)
import           Data.Text.Encoding                     (decodeUtf8, encodeUtf8)
import           Database.SQLite.Simple
import           Lucid
import           Lucid.Base
import           Network.HTTP.Client                    (defaultManagerSettings,
                                                         newManager)
import           Network.HTTP.Media                     ((//), (/:))
import           Network.Wai
import           Network.Wai.Handler.Warp
import           Network.Wai.Middleware.Servant.Options
import           Servant
import           Servant.Client
import           Servant.HTML.Lucid

import           Alison.Guests
import           Alison.Middleware                      (middleware)
import           Alison.Types

users :: [User]
users = []
-- users = [User 1 "Isaac" "Newton", User 2 "Albert" "Einstein"]

rsvpHtml :: Html ()
rsvpHtml = div_ (h1_ "RSVP")

encrypted :: Text -> IO (Maybe Text)
encrypted plainText = do
  let byteText = encodeUtf8 plainText
  encryptedText <- hashPasswordUsingPolicy slowerBcryptHashingPolicy byteText
  return $ decodeUtf8 <$> encryptedText

type API =
       "users" :> Get '[JSON] [User]
  :<|> "rsvp" :> Get '[HTML] (Html ())
  :<|> "rsvp" :> ReqBody '[JSON] Guest :> Put '[JSON] NoContent
  -- :<|> "guests" :> Get '[JSON] [Guest]

server :: FilePath -> Server API
server dbFile =
         return users
    :<|> return rsvpHtml
    :<|> postGuest
    -- :<|> getGuests
    where
      postGuest :: Guest -> Handler NoContent
      postGuest jsonGuest = do
        encryptedPassword <- liftIO $ encrypted $ password jsonGuest
        case encryptedPassword of
          Just password -> liftIO $ insertGuests
          Nothing       -> liftIO $ print encryptedPassword
        return NoContent

      -- getGuests :: Handler [Guest]
      -- getGuests = liftIO $ withConnection dbFile $ \conn ->
      --   query_ conn "SELECT * FROM guests" :: IO [Guest]

proxy :: Proxy API
proxy = Proxy

alison :: FilePath -> Application
alison dbFile = serve proxy $ server dbFile

startAlison :: FilePath -> IO ()
startAlison dbFile = do
  run 8080 $ middleware (alison dbFile)
